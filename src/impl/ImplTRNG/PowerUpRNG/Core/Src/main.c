#include "main.h"
#include <stdio.h>
#include <string.h>
#include "stm32f4xx_nucleo.h"
#include "stm32f4xx_hal.h"

#include <stdio.h>
#include <memory.h>
#include <string.h>
#include "sha256.h"

RTC_HandleTypeDef hrtc;
UART_HandleTypeDef huart2;

uint8_t * SRAMpointer = (uint8_t *) 0x20015800;
uint8_t PRINT_MODE = SRAM_PRINT_BIN;

int GEN_COUNT = 500000 ;
int SRAM_BLOCK_SIZE = 512;


int main(void)
{
	HAL_Init();
	SystemClock_Config();

	UART_Init();
	RTC_Init();
	PWR_Init();

	BSP_LED_Init(LED2);
	BSP_PB_Init(BUTTON_KEY,BUTTON_MODE_GPIO);

	//print("\r\nTURNED ON. Init. done.\r\n");

	//Checks if MCU was started or woke from standby
	if (__HAL_PWR_GET_FLAG(PWR_FLAG_SB) != RESET  )
	{
		// generates random number until desired amount of number is generated.
		uint32_t counter = HAL_RTCEx_BKUPRead(&hrtc,RTC_BKP_DR1);
		if(counter < GEN_COUNT)
		{
			__HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);
			generate();
		}
	}


	int up = 0;
	while(1)
	{
		// waits for user button to be pressed.
		if(BSP_PB_GetState(BUTTON_KEY) == GPIO_PIN_RESET && !up)
		{
			up = 1;
		}
		else if(up && BSP_PB_GetState(BUTTON_KEY) == GPIO_PIN_SET )
		{
			//enter stand-by to reset SRAM content.
			up = 0;
			HAL_RTCEx_BKUPWrite(&hrtc,RTC_BKP_DR1,0);
			enterStandby();
		}
	}
}

/*
 * Routine to generate a random number. Function reads block of memory and sents its hash to PC.
 */
void generate(void)
{

	BYTE sramBlock[SRAM_BLOCK_SIZE];
	memcpy(sramBlock, SRAMpointer, SRAM_BLOCK_SIZE);
	printHashed(sramBlock);
	counterUp();
	enterStandby();

	// not accessible
	while(1){
	  print("Not Accessible\r\n");
	  HAL_Delay(500);
	  BSP_LED_Toggle(LED2);
	}
}

/*
 * Computes and sents hash of input block to PC
 */
void printHashed(BYTE * buff)
{
	SHA256_CTX ctx;
	BYTE buf[SHA256_BLOCK_SIZE];
	sha256_init(&ctx);
	sha256_update(&ctx, buff, SRAM_BLOCK_SIZE);
	sha256_final(&ctx, buf);

	HAL_UART_Transmit(&huart2, buf, SHA256_BLOCK_SIZE, 100);
}

/*
 * Prints char array.
 */
void print(char * msg)
{
	HAL_UART_Transmit(&huart2, (uint8_t*) msg, strlen(msg), 100);
}

/*
 * Increases a counter of generated numbers.
 */
void counterUp(void)
{
	uint32_t counter = HAL_RTCEx_BKUPRead(&hrtc,RTC_BKP_DR1) + 1 ;
	HAL_RTCEx_BKUPWrite(&hrtc,RTC_BKP_DR1,counter);
}


/*
 * Set WakeUpTimer and enters stand-by mode.
 */
void enterStandby(void)
{
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
	__HAL_RTC_WAKEUPTIMER_CLEAR_FLAG(&hrtc, RTC_FLAG_WUTF);

	if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, WAKEUP_COUNT, RTC_WAKEUPCLOCK_RTCCLK_DIV16) != HAL_OK)
	{
	 Error_Handler();
	}
//	print("Entering STANDBY MODE\n\r");
	HAL_PWR_EnterSTANDBYMode();
}


/**
 * Init. code.
 */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

	RCC_OscInitStruct.OscillatorType		= RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
	RCC_OscInitStruct.LSEState 				= RCC_LSE_ON;
	RCC_OscInitStruct.HSIState 				= RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue 	= RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.LSIState 				= RCC_LSI_ON;
	RCC_OscInitStruct.PLL.PLLState 			= RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource 		= RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM 				= 16;
	RCC_OscInitStruct.PLL.PLLN 				= 336;
	RCC_OscInitStruct.PLL.PLLP 				= RCC_PLLP_DIV4;
	RCC_OscInitStruct.PLL.PLLQ 				= 2;
	RCC_OscInitStruct.PLL.PLLR 				= 2;

	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	RCC_ClkInitStruct.SYSCLKSource 	 = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider	 = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.ClockType 	 = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
		  	  	  	  	  	  	  	   |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
	{
		Error_Handler();
	}
}

void PWR_Init(void)
{
	HAL_PWR_EnableBkUpAccess();
	__HAL_RCC_BKPSRAM_CLK_ENABLE();
	HAL_PWREx_EnableBkUpReg();
}

void RTC_Init(void)
{
	hrtc.Instance 				= RTC;
	hrtc.Init.HourFormat 		= RTC_HOURFORMAT_24;
	hrtc.Init.AsynchPrediv 		= 127;
	hrtc.Init.SynchPrediv		= 255;
	hrtc.Init.OutPut 			= RTC_OUTPUT_DISABLE;
	hrtc.Init.OutPutPolarity	= RTC_OUTPUT_POLARITY_HIGH;
	hrtc.Init.OutPutType 		= RTC_OUTPUT_TYPE_OPENDRAIN;

	if (HAL_RTC_Init(&hrtc) != HAL_OK)
	{
		Error_Handler();
	}
}

void UART_Init(void)
{
	huart2.Instance 			= USART2;
	huart2.Init.BaudRate 		= 115200;
	huart2.Init.WordLength 		= UART_WORDLENGTH_8B;
	huart2.Init.StopBits 		= UART_STOPBITS_1;
	huart2.Init.Parity 			= UART_PARITY_NONE;
	huart2.Init.Mode 			= UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl 		= UART_HWCONTROL_NONE;
	huart2.Init.OverSampling	= UART_OVERSAMPLING_16;

	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
		Error_Handler();
	}
}


void Error_Handler(void)
{
	__disable_irq();
	while (1)
	{
	}
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

