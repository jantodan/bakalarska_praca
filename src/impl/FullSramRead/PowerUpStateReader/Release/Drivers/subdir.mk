################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/stm32f4xx_hal_cryp.c \
../Drivers/stm32f4xx_hal_cryp_ex.c 

OBJS += \
./Drivers/stm32f4xx_hal_cryp.o \
./Drivers/stm32f4xx_hal_cryp_ex.o 

C_DEPS += \
./Drivers/stm32f4xx_hal_cryp.d \
./Drivers/stm32f4xx_hal_cryp_ex.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/%.o: ../Drivers/%.c Drivers/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DUSE_HAL_DRIVER -DSTM32F446xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I"D:/CVUT/Bakalarka/code/trng-stm32f446re/PowerUpStateReader/Drivers" -I"D:/CVUT/Bakalarka/code/trng-stm32f446re/PowerUpStateReader/Drivers/BSP/STM32F4xx-Nucleo" -I"D:/CVUT/Bakalarka/code/trng-stm32f446re/PowerUpStateReader/Drivers/BSP" -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Drivers

clean-Drivers:
	-$(RM) ./Drivers/stm32f4xx_hal_cryp.d ./Drivers/stm32f4xx_hal_cryp.o ./Drivers/stm32f4xx_hal_cryp_ex.d ./Drivers/stm32f4xx_hal_cryp_ex.o

.PHONY: clean-Drivers

