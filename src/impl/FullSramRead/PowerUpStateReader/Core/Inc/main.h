#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

#define LED2_Pin 		GPIO_PIN_5
#define LED2_GPIO_Port 	GPIOA
#define TMS_Pin 		GPIO_PIN_13
#define TMS_GPIO_Port 	GPIOA
#define TCK_Pin 		GPIO_PIN_14
#define TCK_GPIO_Port 	GPIOA

#define SRAM_PRINT_BIN 	0
#define SRAM_PRINT_TEXT	1
#define WAKEUP_COUNT	0xA0

void SystemClock_Config(void);
void PWR_Init(void);
void RTC_Init(void);
void UART_Init(void);

void printStep(void);
void printSRAM(void);
void counterUp(void);
void enterStandby(void);
void print(char * msg);

void Error_Handler(void);

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
