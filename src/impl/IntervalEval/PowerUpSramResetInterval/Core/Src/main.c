#include "main.h"
#include <stdio.h>
#include <string.h>
#include "stm32f4xx_nucleo.h"
#include "stm32f4xx_hal.h"

// ----------------------------------------------------------------- /
// https://www.techiedelight.com/count-set-bits-using-lookup-table/

// Macros to generate the lookup table (at compile-time)
#define B2(n) n, n + 1, n + 1, n + 2
#define B4(n) B2(n), B2(n + 1), B2(n + 1), B2(n + 2)
#define B6(n) B4(n), B4(n + 1), B4(n + 1), B4(n + 2)
#define COUNT_BITS B6(0), B6(1), B6(1), B6(2)

// Lookup table to store the total number of bits set for each index
// in the table. The macro `COUNT_BITS` generates the table.
unsigned int lookup[256] = { COUNT_BITS };

// ----------------------------------------------------------------- /

RTC_HandleTypeDef hrtc;
UART_HandleTypeDef huart2;

uint8_t * SRAMpointer = (uint8_t *) 0x20000000;
uint8_t verbose = 1;

int main(void)
{
	HAL_Init();
	SystemClock_Config();

	UART_Init();
	RTC_Init();
	PWR_Init();

	BSP_LED_Init(LED2);
	BSP_PB_Init(BUTTON_KEY,BUTTON_MODE_GPIO);

	print("\r\nTURNED ON. Init. done.\r\n");


	//Checks if MCU was started or woke from standby
	if (__HAL_PWR_GET_FLAG(PWR_FLAG_SB) != RESET  )
	{
		uint32_t counter = HAL_RTCEx_BKUPRead(&hrtc,RTC_BKP_DR1);
		if(counter < 6000)
		{
			// every 50 measurements increases interval for being in stand-by mode.
			if (counter % 50 == 0){
				print("\r\n");
				int WakeUpCount = HAL_RTCEx_BKUPRead(&hrtc,RTC_BKP_DR2);
				HAL_RTCEx_BKUPWrite(&hrtc,RTC_BKP_DR2,WakeUpCount+2);
				char msg2[14] ;
				sprintf(msg2, "%02X\r\n; ",WakeUpCount);
				print(msg2);
			}
			__HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);
			step();
		}
	}

	int up = 0;
	while(1)
	{
		// waits for user button to be pressed.
		if(BSP_PB_GetState(BUTTON_KEY) == GPIO_PIN_RESET && !up)
		{
			up = 1;
		}
		else if(up && BSP_PB_GetState(BUTTON_KEY) == GPIO_PIN_SET )
		{
			up = 0;

			HAL_RTCEx_BKUPWrite(&hrtc,RTC_BKP_DR1,0);
			HAL_RTCEx_BKUPWrite(&hrtc,RTC_BKP_DR2,0x02);
			writeStep();
		}
	}
}

/*
 * Routine to count set bits and prepare SRAM and counter for standby mode.
 */
void step()
{

	countSRAM();
	writeSRAM();
	counterUp();
	enterStandby();

	// not accessible
	while(1){
		print("Not Accessible\r\n");
		HAL_Delay(500);
		BSP_LED_Toggle(LED2);
	}
}

/*
 * Routine to prepare memory and counter before entering to stand-by mode.
 */
void writeStep(void)
{
	writeSRAM();
	counterUp();
	enterStandby();

	// not accessible
	while(1){
		print("Not Accessible\r\n");
		HAL_Delay(500);
		BSP_LED_Toggle(LED2);
	}
}

/*
 * Counts occurencies of set bits (1) in each byte of memory.
 */
void countSRAM(void)
{
	BSP_LED_On(LED2);
	int count = 0;
	for (int i = 2048; i < 129024; i++){
		count += lookup[SRAMpointer[i]];
	}

	char msg2[14] ;
	sprintf(msg2, "%d; ",count);
	print(msg2);
	BSP_LED_Off(LED2);
}

/*
 * Sets all bits to 1 in memory. Except areas used by this program.
 */
void writeSRAM(void)
{
	BSP_LED_On(LED2);
	print("Write 0xFF\r\n");
	for (int i = 2048 ; i < 129024 ; i++){
		BSP_LED_Toggle(LED2);
		SRAMpointer[i] = 0xFF;
	}
	print("Write done.\r\n");
	BSP_LED_Off(LED2);
}

/*
 * Increases a counter of measurement.
 */
void counterUp(void)
{
	uint32_t counter = HAL_RTCEx_BKUPRead(&hrtc,RTC_BKP_DR1) + 1 ;
	HAL_RTCEx_BKUPWrite(&hrtc,RTC_BKP_DR1,counter);
}


/*
 * Prints char array.
 */
void print(char * msg)
{
	if(verbose)
		HAL_UART_Transmit(&huart2, (uint8_t*) msg, strlen(msg), 1152000);
}

/*
 * Set WakeUpTimer and enters stand-by mode.
 */
void enterStandby(void)
{
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
	__HAL_RTC_WAKEUPTIMER_CLEAR_FLAG(&hrtc, RTC_FLAG_WUTF);

	uint32_t wakeUpCounter = HAL_RTCEx_BKUPRead(&hrtc,RTC_BKP_DR2);
	if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, wakeUpCounter, RTC_WAKEUPCLOCK_RTCCLK_DIV16) != HAL_OK)
	{
	 Error_Handler();
	}
	print("Entering STANDBY MODE\n\r");
	HAL_PWR_EnterSTANDBYMode();
}


/**
 * Init. code.
 */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

	RCC_OscInitStruct.OscillatorType		= RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
	RCC_OscInitStruct.LSEState 				= RCC_LSE_ON;
	RCC_OscInitStruct.HSIState 				= RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue 	= RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.LSIState 				= RCC_LSI_ON;
	RCC_OscInitStruct.PLL.PLLState 			= RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource 		= RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM 				= 16;
	RCC_OscInitStruct.PLL.PLLN 				= 336;
	RCC_OscInitStruct.PLL.PLLP 				= RCC_PLLP_DIV4;
	RCC_OscInitStruct.PLL.PLLQ 				= 2;
	RCC_OscInitStruct.PLL.PLLR 				= 2;

	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	RCC_ClkInitStruct.SYSCLKSource 	 = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider	 = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.ClockType 	 = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
		  	  	  	  	  	  	  	   |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
	{
		Error_Handler();
	}
}

void PWR_Init(void)
{
	HAL_PWR_EnableBkUpAccess();
	__HAL_RCC_BKPSRAM_CLK_ENABLE();
	HAL_PWREx_EnableBkUpReg();
}

void RTC_Init(void)
{
	hrtc.Instance 				= RTC;
	hrtc.Init.HourFormat 		= RTC_HOURFORMAT_24;
	hrtc.Init.AsynchPrediv 		= 127;
	hrtc.Init.SynchPrediv		= 255;
	hrtc.Init.OutPut 			= RTC_OUTPUT_DISABLE;
	hrtc.Init.OutPutPolarity	= RTC_OUTPUT_POLARITY_HIGH;
	hrtc.Init.OutPutType 		= RTC_OUTPUT_TYPE_OPENDRAIN;

	if (HAL_RTC_Init(&hrtc) != HAL_OK)
	{
		Error_Handler();
	}
}

void UART_Init(void)
{
	huart2.Instance 			= USART2;
	huart2.Init.BaudRate 		= 115200;
	huart2.Init.WordLength 		= UART_WORDLENGTH_8B;
	huart2.Init.StopBits 		= UART_STOPBITS_1;
	huart2.Init.Parity 			= UART_PARITY_NONE;
	huart2.Init.Mode 			= UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl 		= UART_HWCONTROL_NONE;
	huart2.Init.OverSampling	= UART_OVERSAMPLING_16;

	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
		Error_Handler();
	}
}


void Error_Handler(void)
{
	__disable_irq();
	while (1)
	{
	}
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{

}
#endif /* USE_FULL_ASSERT */

