readme.txt ....................................... stručny popis obsahu CD
data
	blocks128b.csv ......................... entropie 128b blokov pamäte
	blocks1024b.csv........................ entropie 1024b blokov pamäte
	blocks4096b.csv........................ entropie 4096b blokov pamäte
	blocks8192b.csv........................ entropie 8192b blokov pamäte
	finalAnalysisReport.txt ....................... Výsledky testov NIST
	fullsram1024.bin............................. 1024 kópii SRAM pamäte
	means1024.csv.................. priemery hodnôt buniek SRAM z meraní
scripts
	entropy.ipynb.......................... výpočet entropie SRAM pamäte
	entropyBlocks.ipynb ......... vyhodnotenie entropie v blokoch pamäte
	probabilities.ipynb...................... výpočet priemerných hodnôt
src
	impl ................................... zdrojové kódy implementácie
		FullSramRead ............... Program pre zber dát z celej SRAM
		ImplTRNG.......................... Program implementujúci TRNG
		IntervalEval ...... Program pre odhad dĺžky stand-by intervalu
	thesis ....................... zdrojová forma práce vo formáte LATEX
text
	BP Jantosovic Daniel 2022.pdf............. text práce vo formáte PDF